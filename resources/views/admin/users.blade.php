<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">
                <div class="flex">
                    <div class="flex-auto text-2xl mb-4 text-green-500">Users Administration</div>                                                    
                </div>
                <table class="w-full text-md rounded mb-4">
                    <thead>
                    <tr class="border-b">
                        <th class="text-left p-3 px-5">Id</th>
                        <th class="text-left p-3 px-5">Name</th>
                        <th class="text-left p-3 px-5">Email</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr class="border-b hover:bg-orange-100">
                            <td class="p-3 px-5">
                                {{$user->id}}
                            </td>
                            <td class="p-3 px-5">
                                {{$user->name}}
                            </td>
                            <td class="p-3 px-5">
                                {{$user->email}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="flex-auto text-left m-4 p-6">
                        <a href="/admin" class="bg-gray-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" style="background-color:#66bbe3;">Volver</a>
                </div>
            </div>
        </div>
    </div>

    </x-app-layout>